#ifndef VVOD_H
#define VVOD_H

#include <QMainWindow>
#include "grafic.h"
#include "data.h"
#include <QDebug>
namespace Ui {
class Vvod;
}

class Vvod : public QMainWindow
{
    Q_OBJECT

public:
    explicit Vvod(QWidget *parent = 0);
    ~Vvod();
     Data *vvod_data = 0;
 signals:
     void sendData(QString str1,QString str2, QString str3,QString str4 );//сигнал отправления данных на форму 2
    // void sendData(QString str2);
     // void sendData(QString str3);

private slots:
   // void on_pushButton_2_clicked();
    void onButtonSend();//слот отправления данных на форму 2

private:
    Ui::Vvod *ui;
    Grafic *window;

};

#endif // VVOD_H
