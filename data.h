#ifndef DATA
#define DATA
#include <QGraphicsPixmapItem>
#include <QWidget>
#include <QGraphicsView>
#include <QGraphicsScene>
#include <QGraphicsItem>
#include "data.h"
#include <QTimer>
#include <QDebug>
struct Data
{
    Data()
    {
        speed = 0;
        i=5;
        xspeed =0;
        pressed=10;
        tspeed=0;
        speedleb=0;
        flag_r_l=0;
        x=0;
        y=0;
        l_kab=0;
        n_max=0;
        D=0;
        d=0;
        B=0;
        R_tec=0;
        aks=0;
        flag_ask=0;
        flag_avtomat=0;
        naprav_vrach_avtomat=2;
        speedleb_avtomat=0;

    }

    float speed; //скорость ПА
    float tspeed; //скорость течения
    int i;//индикатор направления перемещения ПА:вверх, вниз, вправо, влево
    float xspeed;//направление движения ПА по x (скорость перемещения)
    int pressed;//индикатор
    int speedleb;//число оборотов лебёдки в минуту
    int  flag_r_l;//направление вращения:0-нет, 1- лево, 2-право
    float x;//координата x ПА
    float y;//координата y ПА
    double l_kab;//длина кабеля
    int n_max;//максимальное число слоёв намотки кабеля на барабан
    double D;//диаметр барабана лебёдки
    double d;//диаметр кабеля
    double B;//длина барабана
    double l_tec;//длина свободного конца кабеля
    double R_tec;//текущий радиус барабана с кабелем
    double aks;//ускорение ПА
    int flag_ask;//флаг ускорение нажато:1-нажато, 0-отжато
    int flag_avtomat;//флаг автоматического управления лебёдкой
    float naprav_vrach_avtomat;//направление вращения лебёдки при автоматизированном режиме 0 влево 1 вправо
    float speedleb_avtomat;//линейная скорость лебедки с учетом движения ПА, с учетом течения и направления
    int key;//показатель, что клавиатура нажата: 1- нажата, 0-отжата;
};

#endif // DATA

