#include "grafic.h"
#include "ui_grafic.h"
#include <QCloseEvent>
#include <QKeyEvent>
#include <QValidator>
#include <QLineEdit>
#include <QCloseEvent>
#include <QPixmap>
#include <QPainter>
#define SPEED 3
#define KOEF_PROV_MAX 1.2
#define KOEF_PROV_MIN 1.0
#define KOEF_PROV_STEP 0.01
#define VELOCITY_TECH_MAX 2
#define VELOCITY_TECH_STEP 0.1
#define R_BARABAN 0.15
#include "data.h"
#include <math.h>
#include <vector>
#include <string>
#include <iostream>

int counter =0;
//----------------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------------------
//ЛЕБЁДКА
  LEB::LEB(Data *data):QGraphicsPixmapItem(0)
{
    setPixmap(QPixmap(":/img1"));
    setPos(0,0);//для смещения СК в нужное место
    setFlag(QGraphicsItem::ItemIsFocusable);

}
/*void LEB::advance(int phase)//9 перемещение ПА
{
    if(phase){
        //QPointF location = this ->pos();
       // setPos(mapToParent(0,-(speed)));
        //moveBy(pa_data->xspeed,yspeed);

    }
}*/


//----------------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------------------
//ПУЗЫРИКИ
Bol::Bol(int xspread):QGraphicsPixmapItem(0)
{
    setPixmap(QPixmap(":/img4.png"));
    setPos(rand()%(xspread-pixmap().width()),0);//для смещения СК в нужное место

}
void Bol::advance(int phase)//5 указываем условие, чтобы не двигался в 2 раза быстрее
{
    if (phase){
        moveBy(0,yspeed);//по x смещения нет , по y -yspeed
    }
}
//----------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------------------
//ПА

PA::PA(int sceneHeight, Data *data):QGraphicsPixmapItem(0) //7
{
    setPixmap(QPixmap(":/img2.png"));

    pa_data = data;

    setPos(40,sceneHeight - pixmap().height()-40);//сдвинуть СК ПА
    setFlag(QGraphicsItem::ItemIsFocusable);//нужно установить фокус на корабле
    setFocus();

    //std::cerr << "OK\n";

}
void PA::advance(int phase)//9 перемещение ПА
{
    if(phase){
        moveBy(pa_data->xspeed,yspeed);

   pa_data->x=x();
    pa_data->y=y();}



}
void PA::keyPressEvent(QKeyEvent *event)//10
{
    //qDebug() << pa_data->speed << endl;

    switch (event->key()) {
    case Qt::Key_Left: //если ПА движется влево
    {
        pa_data->key=1;
        if (pa_data->pressed==0)//если течения нет
                  {
                    pa_data->xspeed = (-10)*(pa_data->speed);}//скорость ПА равна заданной скорости;
       if (pa_data->pressed==1)// если течение влево
                pa_data->xspeed = (-10)*(pa_data->speed+ pa_data->tspeed);//к скорости ПА прибавляем скорость течения
       if (pa_data->pressed==2)//если течение вправо
                 pa_data->xspeed = (-10)*(pa_data->speed- pa_data->tspeed);//вычетаем из скорости ПА скорость течения
         pa_data->i=0;
       //  qDebug()<<pa_data->xspeed;
         pa_data->speedleb_avtomat=abs(pa_data->xspeed);
    }

        break;
    case Qt::Key_Right:
    {
             pa_data->key=1;
            if (pa_data->pressed==0)//если течения нет
            pa_data->xspeed  = 10*(pa_data->speed);//скорость ПА равна заданной скорости;
if (pa_data->pressed==1)// если течение влево
        pa_data->xspeed = (10)*(pa_data->speed- pa_data->tspeed);//из скорости ПА вычетаем скорость течения
if (pa_data->pressed==2)//если течение вправо
         pa_data->xspeed = (10)*(pa_data->speed+ pa_data->tspeed);//прибавляем к скорости ПА скорость течения

        pa_data->speedleb_avtomat=abs(pa_data->xspeed);


        pa_data->i=1;}
       break;
    case Qt::Key_Up: //если ПА движется вверх
    {  pa_data->key=1;

            yspeed = -10*(pa_data->speed);

       if (pa_data->pressed==1)// если течение влево
       {
             pa_data->xspeed = (-10)*( pa_data->tspeed);
             }
       if (pa_data->pressed==2)// если течение вправо
            {
             pa_data->xspeed = (10)*( pa_data->tspeed);
           }

        pa_data->speedleb_avtomat=sqrt(pa_data->tspeed*pa_data->tspeed+yspeed*yspeed);
        pa_data->i=2;}
       break;

     case Qt::Key_Down: // если ПА движется вниз
    {  pa_data->key=1;
         yspeed = 10*(pa_data->speed);
         if (pa_data->pressed==1)// если течение влево
         {
               pa_data->xspeed = (-10)*( pa_data->tspeed);
               }
         if (pa_data->pressed==2)// если течение вправо
              {
               pa_data->xspeed = (10)*( pa_data->tspeed);
             }

 pa_data->speedleb_avtomat=sqrt(pa_data->tspeed*pa_data->tspeed+yspeed*yspeed);
         pa_data->i=3;}
        break;

    default:
        break;
    }
}
void PA::keyReleaseEvent(QKeyEvent *event) //ПА останавливается, если кнопки клавиатуры отжаты
{ pa_data->key=0;
    switch (event->key()) {
    case Qt::Key_Left:
       pa_data->xspeed = 0;

        break;
    case Qt::Key_Right:
         pa_data->xspeed = 0;
       break;
    case Qt::Key_Up:
        yspeed = 0;pa_data->xspeed = 0;
       break;

     case Qt::Key_Down:
         yspeed = 0;pa_data->xspeed = 0;
        break;

    default:
        break;
    }
}

//---------------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------
//Grafic


Grafic::Grafic(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Grafic)
{
    ui->setupUi(this);

    grafic_data = new Data;

    // connect(dial_2,SIGNAL(valueChanged(float)),n,SLOT(textedited(str)));
    // n->setText(QString::number(a));

    scene = new QGraphicsScene(0,0,430,870,this);//1 создаём сцену, параметр область сцены(прямоугольник), родитель-сама форма,
    //за анимацию отвечает класс QGraphicsScene, вызов метода advance означает смену кадра

    scene->setStickyFocus(true);//11 фокус не будет теряться при щелчке мыши по экрану
    //scene_2->setStickyFocus(true);

    scene->setBackgroundBrush(QBrush(Qt::blue));//кисть заднего фона, цвет синий

    ui->graphicsView->setScene(scene);//1устанавливаем сцену для отображения
    scene->addRect(scene->sceneRect());//1добавляем в сцену графический элемент(прямоугольник размером со сцену)
    pa = new PA(scene->height(), grafic_data);
    //pa->x;
    scene->addItem(pa);//7 добавляем ПА
    //pa->show();
//------------------------------------------------
   scene_2= new QGraphicsScene(0,0,100,100);
   ui->graphicsView_2->setScene(scene_2);
    leb= new LEB(grafic_data);
   scene_2->addItem(leb);
 //  ui->graphicsView_2->setStyleSheet("background");
 scene_2->setBackgroundBrush(QBrush(Qt::blue));


    animationTimer=new QTimer(this);
    connect(animationTimer,SIGNAL(timeout()),scene, SLOT(advance()));//2 связь сигнал срабатывания таймера со слотом сцены:
    //каждое срабатывание таймера приводит к смене кадра
    animationTimer->start(1000/60);//2 60 кадров в секунду
    generatorTimer = new QTimer(this);
    connect(generatorTimer,SIGNAL(timeout()),this,SLOT(onGenerate())); //4 связываем сигнал таймера генератора со слотом созданния пузырика
    generatorTimer->start(1000);//запуск таймера генератора объектов



    connect (animationTimer,SIGNAL(timeout()),this, SLOT(distance()));//определение расстояния между ПА и лебедкой каждый раз,


    connect(animationTimer,SIGNAL(timeout()),this, SLOT(progress()));//изменение значений прогрессбаров
    ui->lineEdit->setText("1.05");//задание первоначального значения коэффициента провисания кабеля
    grafic_data->pressed=0;
    ui->groupBox_2->setEnabled(false);//изначально отключена возможность задавать скорость течения


     connect(animationTimer,SIGNAL(timeout()),this,SLOT(movelebedka())); //связываем сигнал таймера с вращением лебёдки
     connect(animationTimer,SIGNAL(timeout()),this, SLOT(distance()));
    //ui->leb->setPixmap(QPixmap(":/img1.jpg"));//загружаем лебёдку
    // ui->progressBar_3->setValue(50);
     //connect(this,SIGNAL(sendData(QString)),)
     connect (animationTimer,SIGNAL(timeout()),this, SLOT(n_sloi()));//определение числа слоев намотки
   connect(animationTimer,SIGNAL(timeout()),this, SLOT(move_aksel()));//движение с ускорением
    connect(animationTimer,SIGNAL(timeout()),this, SLOT(movelebedka_avtom()));//вращение лебёдки при
    //автоматическом управлении

}

Grafic::~Grafic()
{
    delete ui;
}

void Grafic::recieveData(QString str1,QString str2,QString str3,QString str4)
{
   // QStringList sarr[3];
            // sarr=str.split(' ');
     //scanf(vectorInString,str,vec.x, vec.y, vec.z);
      ui->lineEdit_5->setText(str1);
      ui->lineEdit_15->setText(str2);
      ui->lineEdit_16->setText(str3);
      ui->lineEdit_17->setText(str4);
     // qDebug()<<str1;


      double l;
      double l_kab;
      double B;
      double D;
      double d;
      l=0.0;
      l_kab=(str1).toDouble();
      D=(str2).toDouble();
      d=(str3).toDouble();
      B=(str4).toDouble();

      grafic_data->D=D;
      grafic_data->d=d;
      grafic_data->B=B;
      grafic_data->l_kab=l_kab;


      int n;
     n=0;
     while (l<=l_kab)
        {
         l=l+3.14*(D+2*n*d)*B/d;
        // qDebug()<<l;
         n=n+1;
        // qDebug()<<n;
     }
     //qDebug()<<n;
    grafic_data-> n_max=n;
   }




void Grafic::distance()//

{//ВЫЧИСЛЕНИЕ ТЕКУЩЕЙ ДИСТАНЦИИ ОТ ЛЕБЁДКИ ДО ПА
    // qDebug()<<grafic_data->x;
    QVariant k = (sqrt((grafic_data->x)*(grafic_data->x)+(grafic_data->y)*(grafic_data->y)))*((ui->lineEdit_5->text()).toDouble())/873;
  //  QVariant k = (sqrt((grafic_data->x)*(grafic_data->x)+(grafic_data->y)*(grafic_data->y)))*873/(ui->lineEdit->text()).toDouble()/grafic_data->l_kab;
   // qDebug()<<k;
    ui->lineEdit_6->setText(QString::number(k.toDouble(),10,2));
    //угловая скорость барабана

  /*  QString tmp;
    tmp=ui->lineEdit_5->text();//получение максимальной длины кабеля
    double t =tmp.toDouble();
    vvod_data->l_kab=t;*/
  //ВЫЧИСЛЕНИЕ ТЕКУЩей длины свободного конца кабеля
    double l_tec=(ui->lineEdit_6->text()).toDouble();
    //определение направления вращения лебёдки в автоматизированном режиме
    if (l_tec> grafic_data->l_tec)//если расстояние уведичивается
    {
        grafic_data->naprav_vrach_avtomat=0;//вращать влево
    };
     if (l_tec < grafic_data->l_tec)

         grafic_data->naprav_vrach_avtomat=1;//вращать вправо



    grafic_data->l_tec=l_tec;




}
void Grafic::n_sloi()//определение числа слоев в настоящий момент времени
{
    double l=0;
   double l_raz=grafic_data->l_kab- grafic_data->l_tec;
//qDebug()<<l_raz;
    int n;
   n=0;
   while (l<=l_raz)
      {
       l=l+3.14*(grafic_data->D+2*n*grafic_data->d)*(grafic_data->B)/(grafic_data->d);

       //qDebug()<<l;
       n=n+1;
       //qDebug()<<n;

   }
//qDebug()<<n;
   ui->line->setText(QString::number(n));

 grafic_data->R_tec=(grafic_data->D+2*n*grafic_data->d)/2;
 ui->lineEdit_14->setText(QString::number(grafic_data->R_tec));
}


void Grafic::progress()
{ QString tmp;
     QString tmp_2;
  QString tmp_3;
  QString tmp_4;
    tmp=ui->lineEdit_6->text();
    tmp_2=ui->lineEdit_7->text();
    tmp_3=ui->line->text();
    tmp_4=ui->lineEdit_14->text();

    double R_max;
    R_max=(grafic_data->D+2*grafic_data->n_max*grafic_data->d)/2;
    //QString t =QString::number(((tmp.toDouble(),10,0)*100.0/5500.0));
   // qDebug()<<t.toInt();
    //qDebug()<<tmp;
     // qDebug()<<(tmp.toDouble());
   ui->progressBar_2->setValue(floor((tmp.toDouble())*100/(ui->lineEdit_5->text()).toDouble()));
   // ui->progressBar_2->setValue(90);
   ui->progressBar_3->setValue(floor((tmp_2.toDouble())*100/12.56));
   ui->light->setValue(floor((tmp_3.toDouble())*100/grafic_data->n_max));
   ui->progressBar_4->setValue(floor((tmp_4.toDouble())*100/R_max ));
}

void Grafic::move_aksel()
{
    if (grafic_data->flag_ask==1)
    {
        grafic_data->speed=grafic_data->speed+grafic_data->aks/100;

    }
    if(grafic_data->flag_ask==0)
    {
        grafic_data->speed=(ui->lineEdit_3->text()) .toDouble();
    }
}




void Grafic::onGenerate()

{
    scene->addItem(new Bol(scene->sceneRect().width()));//3 создание на сцене созданного пузырика
}



void Grafic::on_dial_2_valueChanged(int value)//передача частоты вращения лебёдки
{
    ui->lineEdit_2->setText(QString::number(value));

   if(( ui->pushButton_16->styleSheet()=="background-color:green")or( ui->pushButton_15->styleSheet()=="background-color:green"))

    grafic_data->speedleb =( ui->lineEdit_2->text()).toInt();


}



void Grafic::on_dial_3_valueChanged(int value) //ПЕРЕДАЧА ЗНАЧЕНИЯ СКОРОСТИ ПА
{
    QVariant k = value/100.0;

   // QDoubleValidator* validator= new QDoubleValidator(5.0,10.0,25.0);
    //validator->setNotation(QDoubleValidator::StandardNotation);

    //ui->lineEdit_3->setValidator(new QRegExpValidator(QRegExp("^([1-9][0-9]*|0)(\\.|,)[0-9]{2}"),this));

    QString tmp;

    tmp = QString::number(k.toDouble(),10,2);

    ui->lineEdit_3->setText(tmp);

    grafic_data->speed = tmp.toFloat();
 }

void Grafic::on_dial_4_valueChanged(int value)

{
   QVariant k= value/200.0;
   QString tmp;
   tmp = QString::number(k.toDouble(),10,2);

   ui->lineEdit_4->setText(tmp);

   grafic_data->aks = tmp.toFloat();

}

void Grafic::on_pushButton_clicked()//увеличение коэффициента провисание по нажатию кнопки
{
  QString tmp;
  tmp=ui->lineEdit->text();//получение текущего значения коэффициента с окна ввода
  double t =tmp.toDouble();//перевод в число

  if (t<KOEF_PROV_MAX)//если меньше максимального значения
  {
      ui->lineEdit->setText(QString::number((t+KOEF_PROV_STEP),10,2));//прибавить шаг
  }

}

void Grafic::on_pushButton_2_clicked()//уменьшение коэффициента провисания по нажатию кнопки
{
    QString tmp;
    tmp=ui->lineEdit->text();//получение текущего значения коэффициента с окна ввода
    double t =tmp.toDouble();//перевод в число

    if (t>KOEF_PROV_MIN)//если меньше минимального значения
    {
        ui->lineEdit->setText(QString::number((t-KOEF_PROV_STEP),10,2));//вычесть шаг
    }

}
//ТЕЧЕНИЕ----------------------------------------------------------------
//-----------------------------------------------------------------------------
//----------------------------------------------------------------------------------

void Grafic::on_pushButton_6_pressed()//нажата кнопка течение влево
{

    if (ui->pushButton_6->styleSheet()=="background-color:green")//если кнопка отжата
    {ui->pushButton_6->setStyleSheet("");
         grafic_data->tspeed=0;//выключить скорость течения
       grafic_data->pressed=0;
        ui->groupBox_2->setEnabled(false);//отключить возможность задавать скорость течения
        ui->pushButton_7->setEnabled(true);
    }

                else     //иначе
    {ui->pushButton_6->setStyleSheet("background-color:green");//кнопка загарается зеленой при нажатии
       grafic_data->pressed=1;
       grafic_data->tspeed=(ui->lineEdit_12->text()).toFloat();
     ui->groupBox_2->setEnabled(true);//включить возможность задавать скорость течения
     ui->pushButton_7->setEnabled(false);
}}

void Grafic::on_pushButton_7_pressed()//нажата кнопка течение вправо
{

    if (ui->pushButton_7->styleSheet()=="background-color:green")//если течение вправо отключено
    {ui->pushButton_7->setStyleSheet(""); //выключить кнопку
        ui->groupBox_2->setEnabled(false);//отключить возможность задавать скорость течения
        grafic_data->pressed=0;
        grafic_data->tspeed=0;
       ui->pushButton_6->setEnabled(true);
        /*if (grafic_data->i ==1)  //если ПА двигался вправо
        {
                grafic_data->speed-=(ui->lineEdit_12->text()).toFloat();// отнять от скорости ПА скорость течения если ПА движется вправо
         }
        if (grafic_data->i ==0)//если ПА двигался влево
         grafic_data->speed+=(ui->lineEdit_12->text()).toFloat();//прибавить к скорости ПА скорость течения*/
        }

                else
    {ui->pushButton_7->setStyleSheet("background-color:green");//кнопка загарается зеленой при нажатии
      grafic_data->pressed=2;
      grafic_data->tspeed=(ui->lineEdit_12->text()).toFloat();
       ui->groupBox_2->setEnabled(true);//включить возможность задавать скорость течения}
       ui->pushButton_6->setEnabled(false);
      /*if (grafic_data->i ==1)//если ПА движется вправо
      {
              grafic_data->speed+=(ui->lineEdit_12->text()).toFloat();// прибавить к скорости ПА скорость течения если ПА движется вправо
       }
       if (grafic_data->i ==0)//если ПА движется влево
              { grafic_data->speed-=(ui->lineEdit_12->text()).toFloat();}//вычесть из скорости ПА скорость течения
      if ((grafic_data->i ==0) and ((ui->lineEdit_12->text()).toFloat()< grafic_data->speed))
            grafic_data->speed-=(ui->lineEdit_12->text()).toFloat();

        if ((grafic_data->i ==0) and ((ui->lineEdit_12->text()).toFloat()> grafic_data->speed))
        {
            grafic_data->speed=(ui->lineEdit_12->text()).toFloat()-grafic_data->speed;
            // grafic_data->xspeed = 10*(grafic_data->speed);}
    }*/}}



void Grafic::on_pushButton_5_clicked()//увеличение скорости течения
{


    QString tmp;
    tmp=ui->lineEdit_12->text();//получение текущего значения коэффициента с окна ввода


    if (pa->pa_data->tspeed<VELOCITY_TECH_MAX)//если меньше максимального значения
    {
        pa->pa_data->tspeed+=VELOCITY_TECH_STEP;//перевод в число
        ui->lineEdit_12->setText(QString::number((pa->pa_data->tspeed),10,1));//прибавить шаг

    }




}

void Grafic::on_pushButton_4_clicked() //уменьшение скорости течения
{
    QString tmp;
    tmp=ui->lineEdit_12->text();//получение текущего значения коэффициента с окна ввода
    double t =tmp.toDouble();//перевод в число

    if (t>0)//если меньше минимального значения
    {
        ui->lineEdit_12->setText(QString::number((t-VELOCITY_TECH_STEP),10,1));//вычесть шаг
    }
}


void Grafic::on_lineEdit_12_cursorPositionChanged(int arg1, int arg2)//получение скорости течения
{
    grafic_data->tspeed=(ui->lineEdit_12->text()).toFloat();
   // qDebug()<<grafic_data->tspeed;

}
// РУЧНОЙ-АВТОМАТИЧЕСКИЙ====================================
//------------------------------------------------------------------
//---------------------------------------------------------------------

void Grafic::on_pushButton_11_clicked()//кнопка РУЧНОЙ
{
    if (ui->pushButton_11->styleSheet()=="background-color:green")//
    {ui->pushButton_11->setStyleSheet("");
        ui->groupBox_13->setEnabled(true);
                  }

                else
    {ui->pushButton_11->setStyleSheet("background-color:green");//кнопка загарается зеленой при нажатии
      ui->groupBox_13->setEnabled(false);} // блокируется окно автоматический
}

void Grafic::on_pushButton_12_clicked() //нажатие кнопки автоматический
{
    if (ui->pushButton_12->styleSheet()=="background-color:green")//
    {ui->pushButton_12->setStyleSheet("");
        ui->groupBox->setEnabled(true);
        grafic_data->flag_avtomat=0;
                  }

                else
    {ui->pushButton_12->setStyleSheet("background-color:green");//кнопка загарается зеленой при нажатии
      ui->groupBox->setEnabled(false);// блокируется окно ручной
      grafic_data->flag_avtomat=1;
    }
}

//ЗАКРЫТИЕ ФОРМЫ=================================================
//=================================================================
//===============================================================



void Grafic::on_pushButton_3_clicked() //ЗАКРЫТИЕ ФОРМЫ
{
 QApplication::exit(); // QObject::connect(ui->pushButton_3,SIGNAL(clicked(bool)),&ui,SLOT(quit()));
}
//ФИКСАЦИЯ-РАСФИКСАЦИЯ ЛЕБЁДКИ======================================
//========================================================================
//============================================================================
void Grafic::on_pushButton_9_clicked()//кнопка фиксации лебёдки ФИКСАЦИЯ ЛЕБЁДКИ
{
    if (ui->pushButton_9->styleSheet()=="background-color:green")//если расфиксируем лебёдку
    {ui->pushButton_9->setStyleSheet("");
        ui->groupBox_3->setEnabled(true);//включение возможности менять параметры
        ui->groupBox_4->setEnabled(true);
         ui->groupBox_6->setEnabled(true);
                  }

                else
    {ui->pushButton_9->setStyleSheet("background-color:green");//кнопка загарается зеленой при нажатии
        ui->groupBox_3->setEnabled(false);//отключение возможности менять параметры
        ui->groupBox_4->setEnabled(false);
         ui->groupBox_6->setEnabled(false);
         ui->pushButton_8->setStyleSheet("");





}}

void Grafic::on_pushButton_8_clicked()//кнопка расфиксации лебёдки
{
    if (ui->pushButton_8->styleSheet()=="background-color:green")//если была снята расфиксация
    {ui->pushButton_8->setStyleSheet("");

       //меняем цвет
                  }

                else
    {ui->pushButton_8->setStyleSheet("background-color:green");//кнопка загарается зеленой при нажатии
        ui->pushButton_9->setStyleSheet("");
        ui->groupBox_3->setEnabled(true);//включение возможности менять параметры
        ui->groupBox_4->setEnabled(true);
         ui->groupBox_6->setEnabled(true);


}}

void Grafic::on_pushButton_16_clicked() {}
 // ui->graphicsView_2->rotate(+1);


    /*  QPixm  ap ship(":/img2.jpg");
    QPixmap rotate(ship.size());
    QPainter p(&rotate);
    p.setRenderHint(QPainter::Antialiasing);
    p.setRenderHint(QPainter::SmoothPixmapTransform);
    p.setRenderHint(QPainter::HighQualityAntialiasing);
    p.translate(rotate.size().width()/2,rotate.size().height()/2);
    p.rotate(counter);
    p.translate(-rotate.size().width()/2,-rotate.size().height()/2);
    p.drawPixmap(0,0,ship);
    p.end();
    ui->leb->setPixmap(rotate);*/

// ВРАЩЕНИЕ ЛЕБЕДКИ=========================================
//===========================================================
//==================================================================
//-----------------------------------------------------------
//-------------------------------------------------------------
//-----------------------------------------------------------------

void Grafic::movelebedkaleft()
{
    //ui->graphicsView_2->rotate((-1)*( ui->lineEdit_2->text()).toInt()/10.0);
}
void Grafic::movelebedka()//слот вращения лебёдки

{double anglespeed=(( ui->lineEdit_2->text()).toInt())*2*3.14/60;//угловая скорость вращения лебёдки
    if (grafic_data->flag_avtomat==0)

    {
 if  (grafic_data->flag_r_l==1)
 { ui->lineEdit_7->setText(QString::number(anglespeed));
     qDebug()<<anglespeed;
     ui->lineEdit_13->setText(QString::number(anglespeed*grafic_data->R_tec));//передача линейной скорости вращения лебёдки

    ui->graphicsView_2->rotate( ( ui->lineEdit_2->text()).toInt()/10.0);}//изменение вращения на угол 1 оборот в минуту соответствует
    //1/10 градусу, 120 оборотов в минуту соответствует 10 градусам
 if  (grafic_data->flag_r_l==2)
    {ui->graphicsView_2->rotate(- ( ui->lineEdit_2->text()).toInt()/10.0);
     ui->lineEdit_7->setText(QString::number(anglespeed));
  ui->lineEdit_13->setText(QString::number(anglespeed*grafic_data->R_tec));}
 if  (grafic_data->flag_r_l==0)
 {
    ui->graphicsView_2->rotate(0);
    ui->lineEdit_7->setText("0" );
  ui->lineEdit_13->setText("0");}}
}
void Grafic::movelebedka_avtom()
{
   if ((grafic_data->flag_avtomat==1)and ( grafic_data->key==1))
   {
       double anglespeed=(grafic_data->speedleb_avtomat)/grafic_data->R_tec;
          if (grafic_data->naprav_vrach_avtomat==0 )

          {
              ui->graphicsView_2->rotate(-anglespeed*60 /2/3.14/10.0/16);
              ui->lineEdit_7->setText(QString::number(anglespeed/16));
          }
          if (grafic_data->naprav_vrach_avtomat==1 )
               {ui->graphicsView_2->rotate(anglespeed*60 /2/3.14/10.0/16);
               ui->lineEdit_7->setText(QString::number(anglespeed/16));
          }

   }
    if ((grafic_data->flag_avtomat==1)and ( grafic_data->key==0))
   {ui->graphicsView_2->rotate(0);
   ui->lineEdit_7->setText("");
   }

}

void Grafic::on_pushButton_15_clicked(){
}
void Grafic::on_pushButton_7_clicked()
{

}

void Grafic::on_lineEdit_13_cursorPositionChanged(int arg1, int arg2)
{

}

void Grafic::on_pushButton_16_pressed()//ВЫБОРКА КАБЕЛЯ (ЛЕБЁДКА КРУТИТЬСЯ ВПРАВО)
{
    //ВРАЩЕНИЕ ЛЕБЁДКИ ВПРАВО
   // grafic_data->speedleb=(ui->lineEdit_2->text()).toInt();
    if (ui->pushButton_16->styleSheet()=="background-color:green")//если была снята выборка кабеля
       { ui->pushButton_16->setStyleSheet("");

        // grafic_data->speedleb=0;

         grafic_data->flag_r_l=0;
         ui->pushButton_15->setEnabled(true);
           //меняем цвет
                      }
        else
        {ui->pushButton_16->setStyleSheet("background-color:green");//кнопка загарается зеленой при нажатии

         grafic_data->flag_r_l=1;
         ui->pushButton_15->setEnabled(false);
            }
}

void Grafic::on_pushButton_15_pressed()//ТРАВЛЕНИЕ КАБЕЛЯ (ЛЕБЁДКА КРУТИТЬСЯ ВЛЕВО)
//травление лебёдки ВРАЩЕНИЕ ЛЕБЁДКИ ВЛЕВО
    {//grafic_data->speedleb=(ui->lineEdit_2->text()).toInt();
     if (ui->pushButton_15->styleSheet()=="background-color:green")//если было снято травление
            {ui->pushButton_15->setStyleSheet("");
            grafic_data->flag_r_l=0;
            ui->pushButton_16->setEnabled(true);
             //grafic_data->speedleb=0;
               //меняем цвет
                          }
            else
            {ui->pushButton_15->setStyleSheet("background-color:green");//кнопка загарается зеленой при нажатии

              grafic_data->flag_r_l=2;
              ui->pushButton_16->setEnabled(false);


        }


}

void Grafic::on_lineEdit_6_cursorPositionChanged(int arg1, int arg2)

{/*QString tmp;
    tmp=ui->lineEdit_6->text();
    QString t =QString::number(((tmp.toDouble())*100.0/5500.0),10,1);
    qDebug()<<t.toInt();
    ui->progressBar_2->setValue(t.toInt());

*/
    //QString::number((pa->pa_data->tspeed),10,1));
}

void Grafic::on_pushButton_10_clicked()//НАЖАТА КНОПКА РЕГУЛИРОВАНИЯ СКОРОСТИ ПА
{
    if (ui->pushButton_10->styleSheet()=="background-color:green")//если снято регулирование скорости
           {ui->pushButton_10->setStyleSheet("");

           ui->groupBox_8->setEnabled(true);//разрешение управления ускорением
            //grafic_data->speedleb=0;
              //меняем цвет
                         }
           else
           {ui->pushButton_10->setStyleSheet("background-color:green");//кнопка загарается зеленой при нажатии


             ui->groupBox_8->setEnabled(false);


       }
 }


void Grafic::on_pushButton_13_clicked()//НАЖАТА КНОПКА РЕГУЛИРОВАНИЯ УСКОРЕНИЕМ ПА
{
    if (ui->pushButton_13->styleSheet()=="background-color:green")//если снято регулирование ускорением
           {ui->pushButton_13->setStyleSheet("");

           ui->groupBox_9->setEnabled(true);//разрешение управления скоростью
            grafic_data->flag_ask=0;
            //grafic_data->speedleb=0;
              //меняем цвет
                         }
           else
           {ui->pushButton_13->setStyleSheet("background-color:green");//кнопка загарается зеленой при нажатии


             ui->groupBox_9->setEnabled(false);
             grafic_data->flag_ask=1;
}
}
