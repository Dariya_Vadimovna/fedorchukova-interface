#ifndef GRAFIC_H
#define GRAFIC_H
#include <QGraphicsPixmapItem>
#include <QWidget>
#include <QGraphicsView>
#include <QGraphicsScene>
#include <QGraphicsItem>
#include "data.h"
#include <QTimer>
#include <QDebug>
namespace Ui {
class Grafic;
}
class LEB: public QGraphicsPixmapItem{
public:
    LEB( Data *data);//ширина сцены
    Data *leb_data = 0;
public:
   // void advance(int phase); //для анимации

};

//--------------------------------------------------
//-----------------------------------------------------------
//------------------------------------------------------------------
class PA: public QGraphicsPixmapItem{//7 класс ПА
public:
    PA(int sceneHeigth, Data *data);//ширина сцены
    Data *pa_data = 0;

    // QGraphicsItem interface
public:
    void advance(int phase); //для анимации

protected:
    void keyPressEvent(QKeyEvent *event);//8 событие нажатия на клавишу
    void keyReleaseEvent(QKeyEvent *event);//8 событие отпускание клавиши
private:
    int xspeed=0;//создаём член класса перемещения по оси x //8
    int yspeed=0;// перемещение по оси y//8
};


class Bol:public QGraphicsPixmapItem { //2 класс для отображения взвесей
public:
    Bol(int xspread);
public:
    virtual void advance(int phase);//5 сцена дважды вызовет у каждого элемента метод advance: один раз с нулём, второй раз с 1
    //это нужно потому, что при первом проходе мы вычисляем какое-то новое состояние нашего элемента и возможно определяем какие-то столкновения
    //во второй фазе меняем положение графического элемента

private:
    int yspeed=2;//6 параметр скорости по оси y она равна 2
    //protected:
    // virtual void mousePressEvent(QGraphicsSceneMouseEvent*event);
};
/*class Lebed:public QGraphicsPixmapItem {
public:
    Lebed(int xspread);
public:
    virtual void advance(int phase);
private:
    int yspeed=0;


    */


class Grafic : public QWidget
{
    Q_OBJECT

public:
    explicit Grafic(QWidget *parent = 0);
    ~Grafic();

    Data *grafic_data = 0;

    PA *pa;
    LEB *leb;


private slots:
    void recieveData(QString str1, QString str2,QString str3,QString str4);//слот получения данных с первой формы

    void onGenerate(); //3 слот создания шарика
    void n_sloi();//слот определения число слоев намотки кабеля

    void on_dial_2_valueChanged(int value);
    void on_dial_3_valueChanged(int value);

void on_dial_4_valueChanged(int value);
    void movelebedka();//слот перемещения лебёдки
     void movelebedkaleft();//слот перемещения лебёдки
     void distance();//слот расстояния до ПА
  void progress();//слот изменения значения на прогрессбарах
 void move_aksel();//слот движения с ускорением
 void movelebedka_avtom();//вращение лебёдки при автоматическом управлении


    //void on_pushButton_4_clicked();

    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

    void on_pushButton_6_pressed();

    void on_pushButton_7_pressed();



    void on_pushButton_5_clicked();

    void on_lineEdit_12_cursorPositionChanged(int arg1, int arg2);

    void on_pushButton_11_clicked();

    void on_pushButton_12_clicked();

    void on_pushButton_4_clicked();

    void on_pushButton_3_clicked();

    void on_pushButton_9_clicked();

    void on_pushButton_8_clicked();

    void on_pushButton_16_clicked();

    //void on_dial_2_actionTriggered(int action);

    void on_pushButton_15_clicked();

    void on_pushButton_7_clicked();

    void on_lineEdit_13_cursorPositionChanged(int arg1, int arg2);

    void on_pushButton_16_pressed();

    void on_pushButton_15_pressed();

    void on_lineEdit_6_cursorPositionChanged(int arg1, int arg2);

    void on_pushButton_10_clicked();

    void on_pushButton_13_clicked();

private:
    Ui::Grafic *ui;
    QGraphicsScene* scene; //1 создаём объект графической сцены
    QGraphicsScene* scene_2;


    QTimer* animationTimer;//2для анимации
    QTimer* generatorTimer;//2для создания нов ой взвеси
   // int pressed;//кнопка направление течения нажата, номер кнопки
   // int i;//индикатор какая клавиша на компе(вверх, вниз, вправа, влево) нажата

};

#endif // GRAFIC_H
