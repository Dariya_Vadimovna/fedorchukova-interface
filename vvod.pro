#-------------------------------------------------
#
# Project created by QtCreator 2018-12-02T21:00:18
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = vvod
TEMPLATE = app


SOURCES += main.cpp\
        vvod.cpp \
    grafic.cpp

HEADERS  += vvod.h \
    grafic.h \
    data.h

FORMS    += vvod.ui \
    grafic.ui

RESOURCES += \
    images/images.qrc
